# UK Extended, No Dead Keys

This is a Uk extended keyboard layout intended to remove some dead keys when not using AltGr modifier.

Otherwise it works the same as the default layout.

## How to install

1. Open terminal with sudo permissions inside the repository folder
```
# cat uk-ndextd-kb >> /usr/share/X11/xkb/symbols/gb
```
2. With root permission edit /usr/share/X11/xkb/rules/evdev.xml
3. Search English (UK) layout, add a new variant
```xml
<variant>
    <configItem>
        <name>ndextd</name>
        <description>English (UK, Extended, No Dead Keys)</description>
    </configItem>
</variant>
```
4. Select English(UK, Extended, No Dead Keys) as a keyboard layout
